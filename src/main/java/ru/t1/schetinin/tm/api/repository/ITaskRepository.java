package ru.t1.schetinin.tm.api.repository;

import ru.t1.schetinin.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    List<Task> findAllByProjectId(String projectId);

    Task add(Task task);

    void clear();

    Task create(String name, String description);

    Task create(String name);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    int getSize();

}
