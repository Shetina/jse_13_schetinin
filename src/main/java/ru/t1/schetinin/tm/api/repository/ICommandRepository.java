package ru.t1.schetinin.tm.api.repository;

import ru.t1.schetinin.tm.model.Command;

public interface ICommandRepository {

    Command[] getCommands();

}
