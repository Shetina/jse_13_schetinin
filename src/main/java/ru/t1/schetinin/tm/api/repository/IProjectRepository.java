package ru.t1.schetinin.tm.api.repository;

import ru.t1.schetinin.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    Project add(Project project);

    void clear();

    Project create(String name, String description);

    Project create(String name);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    int getSize();

    boolean existsById(String id);
}
